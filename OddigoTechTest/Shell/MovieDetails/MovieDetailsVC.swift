//
//  MovieDetailsVC.swift
//  UnionCoop
//
//  Created by mohamed gamal on 11/14/20.
//

import UIKit
import Cosmos
protocol MovieDetailsView: BaseViewProtocol {
    func configureMovieDetails(imageUrl: String,
                               movieName: String,
                               date: String,
                               rating: Double,
                               overViewText: String)
}

class MovieDetailsVC: BaseVC<MovieDetailsView, MovieDetailsPresenter>, MovieDetailsView {

    // MARK: - OutLet
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var releseDate: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var overviewText: UITextView!
    @IBOutlet weak var ratingView: CosmosView!

    override func setupViews() {
        overviewText.textViewDidChange()
        overviewText.textColor = .white
        overviewText.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight(rawValue: 15))

        ratingView.didFinishTouchingCosmos = { rating in
            self.presenter.setMovieRate(value: rating, movieId: self.presenter.movie.id ?? 0)
        }
    }
    func configureMovieDetails(imageUrl: String,
                               movieName: String,
                               date: String,
                               rating: Double,
                               overViewText: String) {
        movieImage.setImage(with: imageUrl)
        self.movieName.text = movieName
        releseDate.text = date
        ratingLabel.text = "\(rating)"
        self.overviewText.text = overViewText
        self.ratingView.rating = rating
    }

}
