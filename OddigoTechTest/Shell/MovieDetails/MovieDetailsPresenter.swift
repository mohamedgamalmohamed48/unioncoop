//
//  MovieDetailsPresenter.swift
//  UnionCoop
//
//  Created by mohamed gamal on 11/14/20.
//

import Foundation
class MovieDetailsPresenter: BasePresenter<MovieDetailsView> {
    var movie: MoviesResults!

    override func viewDidAttach() {
        self.view.configureMovieDetails(imageUrl: "https://image.tmdb.org/t/p/w500\(movie.posterPath ?? "")",
                                        movieName: movie.title ?? "",
                                        date: "Relese Date: \(movie.releaseDate ?? "")", rating: movie.voteAverage ?? 0.0,
                                        overViewText: movie.overview ?? "")
    }

    func setMovieRate(value: Double, movieId: Int) {
        view.showloader()
        let request = SetMovieRateRequest(rateValue: value, movieId: movieId)
        perform(apiRequest: request,
                outputType: FirstResponse.self)
            .then { [] (data) in
                print(data)
    }.always {  [weak view] in
                view?.hideLoader()
            }
    }
}
