//
//  MovieTableCell.swift
//  UnionCoop
//
//  Created by mohamed gamal on 11/14/20.
//

import UIKit

class MovieTableCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var releseDate: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!

    override func setupViews() {
        self.selectionStyle = .none
        containerView.rightRoundOppositeCorners(radius: 33)
        containerView.layer.masksToBounds = true
    }

    func configure(movies: MoviesResults) {
        self.movieImage.setImage(with: "https://image.tmdb.org/t/p/w500\(movies.backdropPath ?? "")")
        self.movieName.text = movies.originalTitle  ?? ""
        self.releseDate.text = "Release Date: \(movies.releaseDate ?? "")"
        self.ratingLabel.text = "Vote Average: \(movies.voteAverage ?? 0.0)"
    }
}
