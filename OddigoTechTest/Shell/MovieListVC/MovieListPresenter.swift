//
//  MovieListPresenter.swift
//  UnionCoop
//
//  Created by mohamed gamal on 11/14/20.
//

import Foundation
class MovieListPresenter: BasePresenter<MovieListView> {

    private var movies: [MoviesResults] = []
    override func viewDidAttach() {
        getMovies()
    }

    override func tryAgainBtnTappedFromErrorView() {
        getMovies()
    }
    func numberOfRows(at section: Int) -> Int {
        return movies.count
    }

    func numberOfSections() -> Int {
        return 1
    }

    func moviesList(at indexPath: IndexPath) -> MoviesResults {
        movies[indexPath.row]
    }

    func getMovies() {
        view.showloader()
        let request = MovieListRequest()
        perform(apiRequest: request,
                outputType: Movies.self)
            .then { [weak self] (data) in
                self?.movies = data.results ?? []
            }.catch { [weak view] (error) in
                if error.isNoInternet {
                    view?.showErrorMessage(error.message)
                } else {
                    view?.showErrorMessage(error.message)
                }
            }.always {  [weak view] in
                view?.reloadTableView()
                view?.hideLoader()
            }
    }

}
