//
//  MovieListVC.swift
//  UnionCoop
//
//  Created by mohamed gamal on 11/14/20.
//

import UIKit
protocol MovieListView: BaseViewProtocol {
    func reloadTableView()
}
class MovieListVC: BaseVC<MovieListView, MovieListPresenter>, MovieListView {

    // MARK: - OultLet
    @IBOutlet weak var tableView: UITableView!

    // MARK: - LifyCycle
    override func setupViews() {
        self.hideNavigationBar()
        tableViewSetup()
        self.tableView.reloadData()
    }

    private func tableViewSetup() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.register(cellType: MovieTableCell.self)
    }

    func reloadTableView() {
        self.tableView.reloadData()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}

// MARK: tablView Data Source Methods
extension MovieListVC: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSections()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows(at: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MovieTableCell = tableView.dequeueReusableCell(for: indexPath)
        cell.configure(movies: presenter.moviesList(at: indexPath))
        return cell
    }
}

// MARK: tablView Delegate Source Methods
extension MovieListVC: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let movieDetailsVC: MovieDetailsVC = Resolver.resolve(args: presenter.moviesList(at: indexPath))
        presentVC(viewController: movieDetailsVC)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }

    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
       tableView.fadeEdges(with: 1.0)
    }
}
