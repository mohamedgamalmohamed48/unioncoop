//
//  AppDelegate.swift
//  UnionCoop
//
//  Created by mohamed gamal on 10/1/20.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appDelegates: [UIApplicationDelegate]  = [AppConfigurator()]

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window?.makeKey()
        appDelegates.forEach({ _ = $0.application?(application, didFinishLaunchingWithOptions: launchOptions)})

        return true
    }

}
