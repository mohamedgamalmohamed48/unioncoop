//
//  BaseVC.swift
//  UnionCoop
//
//  Created by mohamed gamal on 10/1/20.
//

import Foundation
import UIKit

protocol BaseViewProtocol: AnyObject {
    func showloader()
    func hideLoader()
    func showErrorMessage(_ message: String)
    func hideDefaultErrorView()
    func popVC()
    func dismiss()
    func pop()
    func hideNavigationBar()
}

class BaseVC<ViewProtocol, Presenter>: UIViewController, BaseViewProtocol where Presenter: BasePresenter<ViewProtocol> {

    lazy var presenter: Presenter = {
        let presenter: Presenter = Resolver.resolve(args: presenterArgs)
        presenterArgs = nil
        return presenter
    }()
    open var presenterArgs: Any?

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let view = self as? ViewProtocol else {
            fatalError("Presenter of type \(Presenter.Type.self) need to attach View of type \(ViewProtocol.Type.self)")
        }
        presenter.attach(view: view)
        setupViews()
        localizeViews()
        //addCartNavigationBarLeadingItem()
        setupNavigationBar()
        //addCartNotificationObserver()
        //addSessionExpiredNotificationObserver()
    }

    open func setupViews() {}

    open func setupNavigationBar() {}

    open func localizeViews() {}

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.viewWillAppear()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter.viewWillDisappear()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter.viewDidDisappear()
    }

    func showloader() {
        showAppLoader()
    }

    func hideLoader() {
        hideAppLoader()
    }

    func showErrorMessage(_ message: String) {
        showErrorView( errorSubTitle: message)
    }

    func hideDefaultErrorView() {
        hideErrorView()
    }

    func hideNavigationBar() {
    self.navigationController?.isNavigationBarHidden = true
    }

    override func tryAgainBtnTappedFromErrorView() {
        presenter.tryAgainBtnTappedFromErrorView()
        startTryAgainButtonLoading()
    }

    func popVC() {
        navigationController?.popViewController(animated: true)
    }

    func pushVC(viewController: UIViewController, completion: (() -> Void)? = nil) {
        navigationController?.pushViewController(viewController, animated: true)
    }

    func pop() {
        navigationController?.popViewController(animated: true)
    }

    func presentVC(viewController: UIViewController) {
        present(viewController, animated: true, completion: nil)
    }

    func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }

}
