//
//  APIRequestProviderProtocol.swift
//  UnionCoop
//
//  Created by mohamed gamal on 10/1/20.
//

import Foundation

protocol APIRequestProviderProtocol {
    typealias APIRequestCompletion = (_ result: Result<Data, APIRequestProviderError>) -> Void
    func perform(apiRequest: APIRequestProtocol, completion: @escaping APIRequestCompletion)
}
