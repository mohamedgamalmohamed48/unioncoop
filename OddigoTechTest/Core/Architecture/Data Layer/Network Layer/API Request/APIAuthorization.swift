//
//  APIAuthorization.swift
//  UnionCoop
//
//  Created by mohamed gamal on 10/1/20.
//

import Foundation

enum APIAuthorization {
    case none
    case barerToken
    var authData: Any {
        switch self {
        case .none:
            return []
        case .barerToken:
          let token = ""
            return ["Authorization": "Bearer \(token)"]
        }
    }
}
