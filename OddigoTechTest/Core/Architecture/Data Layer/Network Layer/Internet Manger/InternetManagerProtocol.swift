//
//  InternetManagerProtocol.swift
//  UnionCoop
//
//  Created by mohamed gamal on 10/1/20.
//

import Foundation
protocol InternetManagerProtocol: class {
    func  isInternetConnectionAvailable () -> Bool
}
