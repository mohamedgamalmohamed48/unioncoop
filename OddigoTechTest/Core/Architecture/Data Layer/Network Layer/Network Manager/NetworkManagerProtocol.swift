//
//  NetworkManagerProtocol.swift
//  UnionCoop
//
//  Created by mohamed gamal on 10/1/20.
//

import Foundation
import Promises

protocol NetworkManagerProtocol {
    func perform<T: Codable>(apiRequest: APIRequestProtocol,
                             providerType: APIRequestProviderProtocol,
                             outputType: T.Type) -> Promise<T>
}
