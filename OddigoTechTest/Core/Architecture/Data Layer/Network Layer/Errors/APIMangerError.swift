//
//  APIMangerError.swift
//  UnionCoop
//
//  Created by mohamed gamal on 10/1/20.
//

import Foundation

enum APIManagerError: Error {
    case requestFailed(message: String)
    case errorModel(errorModel: ErrorModel)
    case noInternet(message: String)

    var message: String {
        switch self {
        case .requestFailed(let message):
            return message
        case .errorModel(let errorModel):
            return errorModel.errorDetail ?? ""
        case .noInternet(let message):
            return message
        }
    }
}
//extension Error {
//    var message: String {
//        if let apiError = self as? APIManagerError {
//            return apiError.message
//        } else if let apiError = self as? APIRequestProviderError {
//            return apiError.reason
//
//        } else {
//            let error = self as NSError
//             return error.domain
//        }
//    }
//}
