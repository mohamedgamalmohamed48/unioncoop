//
//  ErrorModel.swift
//  UnionCoop
//
//  Created by mohamed gamal on 10/1/20.
//

import Foundation

struct ErrorModel: Codable {
    let errorDetail: String?
    let code: Int?

    init(code: Int, errorDetail: String) {
        self.code = code
        self.errorDetail  = errorDetail
    }

    enum CodingKeys: String, CodingKey {
           case code
           case errorDetail = "Message"
       }
}
