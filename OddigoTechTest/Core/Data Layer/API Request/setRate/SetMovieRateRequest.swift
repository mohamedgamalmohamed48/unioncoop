//
//  SetMovieRateRequest.swift
//  UnionCoop
//
//  Created by mohamed gamal on 11/15/20.
//

import Foundation

class SetMovieRateRequest: BaseAPIRequest {

    var rateValue: Double
    var movieId: Int
    init(rateValue: Double, movieId: Int) {
        self.rateValue = rateValue
        self.movieId = movieId

        super.init()
        path = "/3/movie/\(movieId)/rating"
        authorization = .none
        method = .post
        queryBody = ["value": rateValue]

    }

    override func queryParams() -> [String: String]? {
        return ["api_key": "fbee24bb5abc818cb1233a9069c27261",
                "guest_session_id": "a8489b74ca729ec2f020298e6de66122"]
    }
}
