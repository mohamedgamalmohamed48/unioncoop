//
//  MovieListRequest.swift
//  UnionCoop
//
//  Created by mohamed gamal on 11/14/20.
//

import Foundation

class MovieListRequest: BaseAPIRequest {

    override init() {
        super.init()
        path = "/4/list/1"
        authorization = .none
        method = .get

    }

    override func queryParams() -> [String: String]? {
        return ["page": "1",
                "api_key": "fbee24bb5abc818cb1233a9069c27261",
                "sort_by": "title.asc"]
    }
}
