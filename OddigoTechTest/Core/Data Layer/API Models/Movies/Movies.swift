//
//  Movies.swift
//  UnionCoop
//
//  Created by mohamed gamal on 11/14/20.
//

import Foundation
// Movies: - Welcome
struct Movies: Codable {
    let id, page: Int?
    let totalResults: Int?
    let objectIDS: [String: String?]?
    let revenue, totalPages: Int?
    let name: String?
    let welcomePublic: Bool?
    let comments: [String: String?]?
    let sortBy, welcomeDescription, backdropPath: String?
    let results: [MoviesResults]?
    let averageRating: Double?
    let runtime: Int?
    let createdBy: CreatedBy?
    let posterPath: String?
}

// MARK: - CreatedBy
struct CreatedBy: Codable {
    let gravatarHash, name, username, id: String?

    enum CodingKeys: String, CodingKey {
        case gravatarHash = "gravatar_hash"
        case name, username, id
    }
}

// MARK: - MoviesResults
struct MoviesResults: Codable {
    let posterPath: String?
    let popularity: Double?
    let voteCount: Int?
    let video: Bool?
    let id: Int?
    let adult: Bool?
    let backdropPath: String?
    let originalLanguage: String?
    let originalTitle: String?
    let genreIDS: [Int]?
    let title: String?
    let voteAverage: Double?
    let overview, releaseDate: String?

    enum CodingKeys: String, CodingKey {
        case posterPath = "poster_path"
        case popularity
        case voteCount = "vote_count"
        case video
        case id, adult
        case backdropPath = "backdrop_path"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case genreIDS = "genre_ids"
        case title
        case voteAverage = "vote_average"
        case overview
        case releaseDate = "release_date"
    }
}

struct FirstResponse: Codable {
    let success: Bool?
    let statusCode: Int?
    let statusMessage: String?

    enum CodingKeys: String, CodingKey {
        case success
        case statusCode = "status_code"
        case statusMessage = "status_message"
    }
}
