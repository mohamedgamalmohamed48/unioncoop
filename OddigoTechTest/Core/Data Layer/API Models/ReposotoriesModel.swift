//
//  ReposotoriesModel.swift
//  UnionCoop
//
//  Created by mohamed gamal on 10/1/20.
//

import Foundation

struct RepositoriesModel: Codable {
    let author, name: String?
    let avatar: String?
    let url: String?
    let repoDesc, language, languageColor: String?
    let stars, forks, currentPeriodStars: Int?
    let builtBy: [BuiltBy]

    enum CodingKeys: String, CodingKey {
        case author, name, avatar, url
        case repoDesc = "description"
        case language, languageColor, stars, forks, currentPeriodStars, builtBy
    }
}

// MARK: - BuiltBy
struct BuiltBy: Codable {
    let username: String
    let href, avatar: String
}
