//
//  MovieListContainer.swift
//  UnionCoop
//
//  Created by mohamed gamal on 11/14/20.
//

import Foundation

extension Resolver {
    static func registerMovieListContainers() {
        register { MovieListVC() }
        register { (_, args) -> MovieListVC in
            let view = MovieListVC()
            view.presenterArgs = args
            return view
        }

        register { (_, _) -> MovieListPresenter in
            let presenter = MovieListPresenter()
            return presenter
        }
    }
}
