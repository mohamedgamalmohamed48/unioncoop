//
//  MovieDetailsContainers.swift
//  UnionCoop
//
//  Created by mohamed gamal on 11/14/20.
//

import Foundation

extension Resolver {
    static func registerMovieDetailsContainers() {
        register { MovieDetailsVC() }
        register { (_, args) -> MovieDetailsVC in
            let view = MovieDetailsVC()
            view.presenterArgs = args
            return view
        }

        register { (_, args) -> MovieDetailsPresenter in
            let presenter = MovieDetailsPresenter()
            presenter.movie = args as? MoviesResults
            return presenter
        }
    }
}
