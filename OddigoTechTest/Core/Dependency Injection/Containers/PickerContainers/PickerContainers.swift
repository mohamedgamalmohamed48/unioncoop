//
//  PickerContainers.swift
//  UnionCoop
//
//  Created by mohamed gamal on 10/1/20.

import Foundation

extension Resolver {
    static func registerPickerContainers() {
        register { (_, args) -> PickerVC in
            let view = PickerVC()
            view.presenterArgs = args
            return view
        }

        register { (_, args) -> PickerPresenter in
            let presenter = PickerPresenter()
            presenter.dataSource = args as! [String]
            return presenter
        }
    }
}
