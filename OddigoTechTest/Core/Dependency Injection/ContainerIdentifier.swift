//
//  Identifiers .swift
//  UnionCoop
//
//  Created by mohamed gamal on 10/1/20.
//

extension ContainerIdentifier {
    public static var local = ContainerIdentifier(id: "Local")
    public static var remote = ContainerIdentifier(id: "remote")
}
