//
//  Resolver+ResolverRegistering.swift
//  UnionCoop
//
//  Created by mohamed gamal on 10/1/20.
//

import Foundation

extension Resolver: ResolverRegistering {

    public static func registerAllServices() {
        registerNetworkLayerContainers()
        registerPickerContainers()
        registerMovieListContainers()
        registerMovieDetailsContainers()
    }

}
