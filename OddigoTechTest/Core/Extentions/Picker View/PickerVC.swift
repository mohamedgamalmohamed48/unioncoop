//
//  PickerVC.swift
//  UnionCoop
//
//  Created by mohamed gamal on 10/1/20.
//

import UIKit

protocol PickerViewDelegate: class {
    func didSelect(_ pickerView: PickerVC, row: Int)
}

protocol PickerView: BaseViewProtocol {
    func didSelectRow(at row: Int)
}

class PickerVC: BaseVC<PickerView, PickerPresenter>, PickerView {

    // MARK: - Outlets
    @IBOutlet private weak var pickerBottomConstraint: NSLayoutConstraint!
    @IBOutlet private weak var pickerView: UIPickerView!
    @IBOutlet private weak var doneButton: UIButton!
    @IBOutlet private weak var containerView: UIView!

    // MARK: - Variables
    weak var delegate: PickerViewDelegate?

    // MARK: - Lifecycle
    override func setupViews() {
        pickerBottomConstraint.constant = (UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0)
        setupPickerView()
    }

    override func localizeViews() {
        doneButton.setTitle("Done",
                                   for: .normal)
    }

    private func setupPickerView() {
        pickerView.delegate = self
        pickerView.dataSource = self
        containerView.layer.cornerRadius = 15
        containerView.clipsToBounds = true
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        pickerBottomConstraint.constant = (UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0)
        doneButton.layer.cornerRadius = 5
    }

    // MARK: - PickerView functions
    func didSelectRow(at row: Int) {
        delegate?.didSelect(self, row: row)
    }

    // MARK: - IBActions
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func doneAction(_ sender: Any) {
        if let selectedRow = presenter.selectedRow() {
            delegate?.didSelect(self, row: selectedRow)
        }
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - Picker View Delegate
extension PickerVC: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        presenter.didSelectComponent(at: row)
    }
}

// MARK: - Picker View Data Source
extension PickerVC: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return presenter.numberOfComponents()
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return presenter.numberOfItems(at: component)
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return presenter.itemForRow(at: row)
    }
}
