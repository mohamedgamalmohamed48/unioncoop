//
//  PickerPresenter.swift
//  UnionCoop
//
//  Created by mohamed gamal on 10/1/20.
//

import Foundation

class PickerPresenter: BasePresenter<PickerView> {

    var dataSource: [String] = []

    private var selectedIndex: Int!

    override func viewDidAttach() {
        selectedIndex = dataSource.count > 0 ?  0 : nil
    }

    func numberOfItems(at component: Int) -> Int {
        dataSource.count
    }

    func itemForRow(at row: Int) -> String {
        dataSource[row]
    }

    func numberOfComponents() -> Int {
        1
    }

    func didSelectComponent(at row: Int) {
        selectedIndex = row
    }

    func selectedRow() -> Int? {
        selectedIndex
    }
}
