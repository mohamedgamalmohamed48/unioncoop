//
//  Errors + Extensions.swift
//  UnionCoop
//
//  Created by mohamed gamal on 11/14/20.
//

import Foundation
extension NSError {
    static let apiFailureError = NSError(domain: "API Failure Error",
                                         code: 404,
                                         userInfo: nil)
    static let cancelError = NSError(domain: "cancel Error",
                                     code: 4000,
                                     userInfo: nil)

    static let noInternet = NSError(domain: "No Internet Error",
                                    code: 5005,
                                    userInfo: nil)
}
extension Error {

    var message: String {
        if let apiError = self as? APIManagerError {
            return apiError.message
        } else if let apiError = self as? APIRequestProviderError {
            return apiError.reason
        } else {
            let error = self as NSError
            return error.domain
        }
    }

    var isNoInternet: Bool {
        message == "No Internet Error"
    }
}
