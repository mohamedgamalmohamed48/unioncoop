//
//  NSObject+Extension.swift
//  UnionCoop
//
//  Created by mohamed gamal on 11/14/20.
//

import Foundation

extension NSObject {
    /// value that represent a className as string value
    static var className: String {
        return String(describing: self)
    }
}
