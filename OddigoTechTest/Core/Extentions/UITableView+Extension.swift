//
//  UITableView+Extensions.swift
//  UnionCoop
//
//  Created by mohamed gamal on 10/1/20.
//

import UIKit

extension UITableView {

    func register<T: UITableViewCell>(cellType: T.Type) {
        let nib = UINib(nibName: cellType.className, bundle: nil)
        self.register(nib, forCellReuseIdentifier: cellType.className)
    }

    func register<T: UITableViewHeaderFooterView>(viewType: T.Type) {
        self.register(viewType, forHeaderFooterViewReuseIdentifier: viewType.className)
    }

    func register<T: UITableViewHeaderFooterView>(nibType: T.Type) {
        let nib = UINib(nibName: nibType.className, bundle: nil)
        self.register(nib, forHeaderFooterViewReuseIdentifier: nibType.className)
    }

    func dequeueReusableCell<T: UITableViewCell>() -> T? {
        let cell = self.dequeueReusableCell(withIdentifier: T.className)
        return cell as? T
    }

    func dequeueReusableHeaderFooter<T: UITableViewHeaderFooterView>() -> T {
        let cell = self.dequeueReusableHeaderFooterView(withIdentifier: T.className)
        return cell as! T
    }

    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        let cell = self.dequeueReusableCell(withIdentifier: T.className, for: indexPath)
        return cell as! T
    }

    func updateEmptyState(emptyView: UIView?) {
        self.backgroundView = emptyView
    }

    func fadeEdges(with modifier: CGFloat) {

        let visibleCells = self.visibleCells

        guard !visibleCells.isEmpty else { return }
        guard let topCell = visibleCells.first else { return }
        guard let bottomCell = visibleCells.last else { return }

        visibleCells.forEach {
            $0.contentView.alpha = 1
        }

        let cellHeight = topCell.frame.height - 1
        let tableViewTopPosition = self.frame.origin.y
        let tableViewBottomPosition = self.frame.maxY

        guard let topCellIndexpath = self.indexPath(for: topCell) else { return }
        let topCellPositionInTableView = self.rectForRow(at: topCellIndexpath)

        guard let bottomCellIndexpath = self.indexPath(for: bottomCell) else { return }
        let bottomCellPositionInTableView = self.rectForRow(at: bottomCellIndexpath)

        let topCellPosition = self.convert(topCellPositionInTableView, to: self.superview).origin.y
        let bottomCellPosition = self.convert(bottomCellPositionInTableView, to: self.superview).origin.y + cellHeight
        let topCellOpacity = (1.0 - ((tableViewTopPosition - topCellPosition) / cellHeight) * modifier)
        let bottomCellOpacity = (1.0 - ((bottomCellPosition - tableViewBottomPosition) / cellHeight) * modifier)

        topCell.contentView.alpha = topCellOpacity
        bottomCell.contentView.alpha = bottomCellOpacity
    }

}

extension UITableViewCell {
    open override func awakeFromNib() {
        setupViews()
    }
    @objc func localizeView() { }
    @objc func setupViews() { }
    @objc func startLoading() { }
    @objc func stopLoading() { }

    func animateWithTransform() {
        self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            self.transform = CGAffineTransform.identity
        }
    }
}
