//
//  UIImageView+Extension.swift
//  UnionCoop
//
//  Created by mohamed gamal on 10/1/20.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView {
    func setImage(with urlString: String) {
        let url = URL(string: urlString)
        self.kf.setImage(with: url,
                         placeholder: UIImage(imageLiteralResourceName: "placeholder"),
                         options: [.transition(.fade(0.5))])
    }
}
