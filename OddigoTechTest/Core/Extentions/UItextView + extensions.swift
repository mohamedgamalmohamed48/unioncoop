//
//  UItextView + extensions.swift
//  UnionCoop
//
//  Created by mohamed gamal on 11/14/20.
//

import UIKit
extension UITextView {
    func textViewDidChange() {
        let mutableAttrStr = NSMutableAttributedString(string: self.text)
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 17
        mutableAttrStr.addAttributes([NSAttributedString.Key.paragraphStyle: style],
                                     range: NSRange(location: 0,
                                                    length: mutableAttrStr.length))
        self.attributedText = mutableAttrStr
    }
}
